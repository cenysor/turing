#! /usr/bin/python3

# Turing machine visualization

import os
import sys
import time

def printfunc(position, state, tape):
    display = ''

    for i in range(int(columns)):
        display += str(tape[(position - (int(columns) // 2)) + i])

    print('\033[H' + ((int(columns) // 2) * ' ') + state + '\n' + display)
    
    time.sleep(1)
            
def machine(program, start, tape):
    
    position = 500
        
    state = start
    
    print('\033[2J')
    
    printfunc(position, state, tape)
    
    while True:
        for i in program:
            if i[0] == state:
                if tape[position] == i[1]:
                    if i[1] != i[2]:
                        tape[position] = i[2]
                        printfunc(position, state, tape)
                    position += i[3]
                    printfunc(position, state, tape)
                    if state != i[4]:
                        state = i[4]
                        printfunc(position, state, tape)
            elif state == 'H':
                return()

def s3s2bb():
    program = [['A', 0, 1, -1, 'B'],
               ['A', 1, 1, 1, 'C'],
               ['B', 0, 1, 1, 'A'],
               ['B', 1, 1, -1, 'B'],
               ['C', 0, 1, 1, 'B'],
               ['C', 1, 1, -1, 'H']]
    
    start = 'A'
    
    tape = []
    
    for i in range(1000):
        tape.append(0)
    
    machine(program, start, tape)
    
def s4s2bb():
    program = [['A', 0, 1, -1, 'B'],
               ['A', 1, 1, 1, 'B'],
               ['B', 0, 1, 1, 'A'],
               ['B', 1, 0, 1, 'C'],
               ['C', 0, 1, -1, 'H'],
               ['C', 1, 1, 1, 'D'],
               ['D', 0, 1, -1, 'D'],
               ['D', 1, 0, -1, 'A']]
    
    start = 'A'
    
    tape = []
    
    for i in range(1000):
        tape.append(0)
    
    machine(program, start, tape)
    
def first():
    program = [['A', ' ', 0, -1, 'B'],
               ['B', ' ', ' ', -1, 'C'],
               ['C', ' ', 1, -1, 'D'],
               ['D', ' ', ' ', -1, 'A']]
    
    start = 'A'
    
    tape = []
    
    for i in range(1000):
        tape.append(' ')
    
    machine(program, start, tape)
    
def copy():
    program = [['A', 0, 0, 0, 'H'],
               ['A', 1, 0, -1, 'B'],
               ['B', 0, 0, -1, 'C'],
               ['B', 1, 1, -1, 'B'],
               ['C', 0, 1, 1, 'D'],
               ['C', 1, 1, -1, 'C'],
               ['D', 0 , 0, 1, 'E'],
               ['D', 1, 1, 1, 'D'],
               ['E', 0, 1, -1, 'A'],
               ['E', 1, 1, 1, 'E']]
               
    start = 'A'
    
    tape = []
    
    for i in range(1000):
        tape.append(0)
        
    tape[500] = 1
    tape[499] = 1
    tape[498] = 1
    
    machine(program, start, tape)
    
def binary():
    program = [['A', ' ', ' ', -1, 'B'],
               ['A', 0, 0, 1, 'A'],
               ['A', 1, 1, 1, 'A'],
               ['B', ' ', 1, 1, 'C'],
               ['B', 0, 1, -1, 'C'],
               ['B', 1, 0, -1, 'B'],
               ['C', ' ', ' ', -1, 'A'],
               ['C', 0, 0, 1, 'C'],
               ['C', 1, 1, 1, 'C']]
               
    start = 'A'
    
    tape = []
    
    for i in range(1000):
        tape.append(' ')
        
    tape[500] = 1
    tape[499] = 1
    tape[498] = 0
    tape[497] = 1
    
    machine(program, start, tape)

rows, columns = os.popen('stty size', 'r').read().split()

while True:
 
    print('''
3-state, 2-symbol busy beaver [b3]
4-state, 2-symbol busy beaver [b4]
Turing's very first example [f]
copy subroutine [c]
binary counter [b]
exit [x]
''')

    ip = input()

    if ip == 'b3':
        s3s2bb()
    elif ip == 'b4':
        s4s2bb()
    elif ip == 'f':
        first()
    elif ip == 'c':
        copy()
    elif ip == 'b':
        binary()
    elif ip == 'x':
        sys.exit()
